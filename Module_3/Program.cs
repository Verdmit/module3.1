﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            #region Task1_Check
            
            string userInput311;
            string userInput312;
            string out322_result;

            var T1 = new Task1();
            int numb1;
            int numb2;
            int Multiplicated;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 1");
            Console.WriteLine("Перемножаем без умножения");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите число 1 для обработки ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            userInput311 = Console.ReadLine();

            numb1 = T1.ParseAndValidateIntegerNumber(userInput311);

            if (numb1 == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ввод неверен. Повторите ввод");
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите число 2 для обработки ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            userInput312 = Console.ReadLine();
            Console.WriteLine("");

            numb2 = T1.ParseAndValidateIntegerNumber(userInput312);

            if (numb2 == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ввод неверен. Повторите ввод");
            }            

            Multiplicated = T1.Multiplication(numb1, numb2);

            out322_result = Convert.ToString(Multiplicated);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Перемножили оба числа. Получилось ");
            Console.WriteLine(Convert.ToString(out322_result) + "\n");

            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task2_Check

            string userInput321;
            int natNum;
            List<int> outT2list;
            bool tryAgain;

            var T2 = new Task2();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 2");
            Console.WriteLine("Натуральные числа");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите количество натуральных чисел ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            userInput321 = Console.ReadLine();

            tryAgain = T2.TryParseNaturalNumber(userInput321, out natNum);

            if (tryAgain)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ввод неверен. Повторите ввод");
            }

            outT2list = new List<int> (T2.GetEvenNumbers(natNum));
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Вот список натуральных чисел :");
            list2ConsoleOut(outT2list);

            void list2ConsoleOut(List<int> list32out)
            {
                Console.WriteLine("\n");

                Console.ForegroundColor = ConsoleColor.Green;
                foreach (int val in list32out)
                {
                    Console.Write(val + " ");
                }
                //Console.ResetColor();
                Console.WriteLine("\n");

            }

            Console.ReadKey();
            Console.Clear();

            #endregion

            #region Task3_Check
            
            string userInput31;
            string userInput32;
            string out323_result;

            var Replace = new Task3();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Task 3");
            Console.WriteLine("Убираем цифру из числа");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите число для обработки ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            var T3 = new Task3();

            userInput31 = Console.ReadLine();
            if ( T3.TryParseNaturalNumber(userInput31, out int number2edit))
            {
                number2edit = Int32.Parse(userInput31);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ввод неверен. Повторите ввод");
            }                   

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите цифру для исключения ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            userInput32 = Console.ReadLine();
            Console.WriteLine("");

            if (T3.TryParseNaturalNumber(userInput32, out int Digit2Remove))
            {
                Digit2Remove = Int32.Parse(userInput32);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ввод неверен. Повторите ввод");
            }

            out323_result = Replace.RemoveDigitFromNumber(Convert.ToInt32(userInput31), Convert.ToInt32(userInput32));

            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Убрали вхождение из числа. Получилось ");
            Console.WriteLine(Convert.ToString(out323_result) + "\n");

            #endregion

            Console.ResetColor();
        }
    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// Использовать этот метод для Парсинга и Валидации корректности ввода 
        /// если пользовательский ввод неверен нужно выбросить исключение
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            try
            {
                return int.Parse(source);
            }
            catch
            {
                throw new ArgumentException();
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int result=0;            
            
            for (int i = 0; i < Math.Abs(num1); i++) result += num2;

            if (num1 == 0 || num2 == 0) 
                return 0;

            if (num1 < 0 && num2 < 0)
                return -result;

            if (num1 < 0 && num2 > 0)
            {
                return -result;
            } 
                return result;            
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            try
            {
                result = int.Parse(input);
                if (result < 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                result = 0;
                return false;
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {            
            List<int> T32;
            int Added = 1;
            T32 = new List<int>();

            if (naturalNumber <= 0)
            {
                return null;
            }

            for (int n = 0; Added <= naturalNumber; n++)
            {
                if (n % 2 == 0)
                {
                    T32.Add(n);
                    Added++;
                }
                else
                {
                    continue;
                }
            }
            return T32;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            try
            {
                result = int.Parse(input);
                if (result < 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                result = 0;
                return false;
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string correction = Convert.ToString(source);
            
            return correction.Replace(Convert.ToString(digitToRemove), "");
        }        
        }
    }
